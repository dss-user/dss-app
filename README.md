# DSS App

The purpose of the DSS app is to dynamically build and display a user-navigateable scroller of a set of baseball games (or other items with similar shape).

## Background

- There is a minimal "component" style framework created to keep the app organized, and minimize/eliminate manual traversal of the DOM.

  Components require a `template` method be defined, and a parent element selector be provided to the components `render` function. This is not the best or most scalable decision but given the constraints and scope of this exercise will be adequate.

- With speed of development in mind, styling will be done via SASS, though a more portable styling system may be more preferential if this was being shipped to various OTT platforms.

#### Components

Components should extend the base `Component` class. They must be initialized with at least one parameter, a parent element which is of the type `HTMLElement`. Components may optionally be provided a second parameter, `props` of the type Object, which can be accessed from inside of the component.

Components has a very basic lifecycle, a `preRender` function which is useful for action such as API calls, and a `render` function which adds the component to the DOM.

A basic example is provided below:

    import Component from "../Component/Component";

    class Example extends Component {
        template() {
            return `<div><h1>${this.props.title || 'Example'}</h1><h3>lorem ipsum</h3></div>`
        }
    }

## Prerequisites

It is assumed the user/system has an environment with the following installed and configured prior to running this project:

- Node.js >= v10.x.x
- Git >= v2.20.x
- Chrome >= v75.x.x
- Mac OSX or unix based machine (though some effort will be made for project to run across platforms)

## Local Development

To run the project locally, clone the repository and in the project root run:

    npm install
    npm start

## Build

To build the project, run npm run:

    npm run build

## Test

To test the project run:

    npm run test
