import App from './App';
import './index.scss';

const app = new App('#root');
app.init();
