import './App.scss';
import { addDays, subDays } from 'date-fns';

// Components
import ItemNav from './Components/ItemNav/ItemNav';

class App {
  static ARROW_UP = 'ArrowUp';
  static ARROW_DOWN = 'ArrowDown';

  set rootEl(selector) {
    this._rootEl = document.querySelector(selector);
  }

  get rootEl() {
    return this._rootEl;
  }

  constructor(rootElSelector) {
    if (!rootElSelector) {
      throw new Error('No root element selector provided to App.');
    }

    this.rootEl = rootElSelector;
    this.bindEvents();
  }

  navs = [];
  activeNavIndex = 0;

  bindEvents() {
    document.addEventListener('keydown', evt =>
      this.handleKeyEvent(evt, 'Down')
    );
  }

  canNavigate() {
    return this.navs.filter(nav => nav.isDetailOpen()).length === 0;
  }

  handleKeyEvent(evt, type) {
    if (!this.canNavigate()) {
      return;
    }

    const navKeyCode = [App.ARROW_UP, App.ARROW_DOWN].find(
      code => code === evt.code
    );
    if (navKeyCode) {
      const functionSignature = `handle${navKeyCode}${type}`;
      if (typeof this[functionSignature] !== 'function') {
        return;
      }
      this[functionSignature]();
    }
  }

  handleArrowUpDown() {
    this.activeNavIndex = this.getNextActiveIndex(--this.activeNavIndex);
    this.setActiveNav();
  }
  handleArrowDownDown() {
    this.activeNavIndex = this.getNextActiveIndex(++this.activeNavIndex);
    this.setActiveNav();
  }

  setActiveNav() {
    this.navs
      .filter((nav, idx) => idx !== this.activeNavIndex)
      .map(nav => nav.setInactive());
    this.navs[this.activeNavIndex].setActive();
  }

  getNextActiveIndex(newIndex) {
    if (newIndex > this.navs.length - 1) {
      return 0;
    }
    if (newIndex < 0) {
      return this.navs.length - 1;
    }
    return newIndex;
  }

  /**
   * Bootstrap initial component creation here.
   */
  async init() {
    const now = new Date();
    const navs = [
      {
        date: now,
        title: "Today's Games"
      },
      {
        date: subDays(now, 1),
        title: "Yesterday's Games"
      },
      {
        date: addDays(now, 1),
        title: "Tomorrows's Games"
      }
    ];

    // Using a for loop instead of an array fn so that elements are
    //  appended/rendered in order.
    for (let i = 0; i < navs.length; ++i) {
      const itemNavComponent = new ItemNav(this.rootEl, navs[i]);
      this.navs.push(itemNavComponent);
      await itemNavComponent.render();
    }

    // Set first nav active.
    this.navs[0].setActive();
  }
}

export default App;
