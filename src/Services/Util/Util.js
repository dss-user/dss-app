// Simple get helper.
function get(key) {
  const obj = this;
  return key.split('.').reduce(function(o, x) {
    return typeof o == 'undefined' || o === null ? o : o[x];
  }, obj);
}

export { get };
