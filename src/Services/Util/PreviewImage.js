const images = require('../../Assets/Images/logos/*.svg');

class PreviewImage {
  static IMAGE_WIDTH = 960;
  static IMAGE_HEIGHT = 540;

  constructor(props) {
    this.props = props;
  }

  async getSrc() {
    const canvas = document.createElement('canvas');
    canvas.width = PreviewImage.IMAGE_WIDTH;
    canvas.height = PreviewImage.IMAGE_HEIGHT;
    const ctx = canvas.getContext('2d');
    await this.drawPreview(ctx);
    return canvas.toDataURL();
  }

  getTeamImageUrl(teamId) {
    return images[teamId];
  }

  getTeamImage(teamId) {
    const image = new Image();
    image.src = this.getTeamImageUrl(teamId);
    image.crossOrigin = 'anonymous';
    return new Promise(res => {
      image.onload = () => res(image);
    });
  }

  async drawPreview(ctx) {
    const { awayTeamId, homeTeamId } = this.props;
    const awayImage = await this.getTeamImage(awayTeamId);
    const homeImage = await this.getTeamImage(homeTeamId);
    const logoSize = 300;
    const awayWidth = logoSize * (awayImage.width / awayImage.height);
    const homeWidth = logoSize * (homeImage.width / homeImage.height);
    const padding = 25;

    // Make a gradient background to separate the logos.
    const gradient = ctx.createLinearGradient(
      20,
      0,
      PreviewImage.IMAGE_WIDTH,
      PreviewImage.IMAGE_HEIGHT
    );
    gradient.addColorStop(0, 'rgba(200,200,200,.5)');
    gradient.addColorStop(0.5, 'rgba(50,50,50, .5)');
    gradient.addColorStop(1, 'rgba(200,200,200, .5)');
    ctx.fillStyle = gradient;

    ctx.fillRect(0, 0, PreviewImage.IMAGE_WIDTH, PreviewImage.IMAGE_HEIGHT);
    ctx.drawImage(awayImage, padding, padding, awayWidth, logoSize);
    ctx.drawImage(
      homeImage,
      PreviewImage.IMAGE_WIDTH - (homeWidth + padding),
      PreviewImage.IMAGE_HEIGHT - (logoSize + padding),
      homeWidth,
      logoSize
    );
  }
}

export default PreviewImage;
