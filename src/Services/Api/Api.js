class Api {
  static BASE_URL = 'https://statsapi.mlb.com/api/v1';

  static get(url) {
    return fetch(Api.BASE_URL + url).then(res => res.json());
  }
}

export default Api;
