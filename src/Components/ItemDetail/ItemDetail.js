import Component from '../Component/Component';
import './ItemDetail.scss';
import { isToday, format, parseISO } from 'date-fns';
import PreviewImage from '../../Services/Util/PreviewImage';

class ItemDetail extends Component {
  static ESCAPE = 'Escape';

  constructor() {
    super(...arguments);
    this.handleKeyDownEvent = this.handleKeyDownEvent.bind(this);
    this.bindEvents();
  }

  bindEvents() {
    document.addEventListener('keydown', this.handleKeyDownEvent);
  }

  unBindEvents() {
    document.removeEventListener('keydown', this.handleKeyDownEvent);
  }

  handleKeyDownEvent(evt) {
    const navKeyCode = [ItemDetail.ESCAPE].find(code => code === evt.code);

    if (navKeyCode) {
      const functionSignature = `handle${navKeyCode}Down`;
      if (typeof this[functionSignature] !== 'function') {
        return;
      }
      this[functionSignature]();
    }
  }

  handleEscapeDown() {
    this.close();
  }

  getTitle() {
    const { props } = this;
    return (
      props.get('content.editorial.recap.mlb.headline') ||
      `${props.get('teams.away.team.name')} at ${props.get(
        'teams.home.team.name'
      )}`
    );
  }

  getText() {
    const { props } = this;

    const articleCopy = props.get('content.editorial.recap.mlb.blurb');
    if (articleCopy) {
      return articleCopy;
    }

    const gameTime = parseISO(props.get('gameDate'));
    return isToday(gameTime)
      ? `Today at ${format(gameTime, 'p')}`
      : format(gameTime, "EEEE 'at' p");
  }

  getImage() {
    const { props } = this;
    const cuts = props.get('content.editorial.recap.mlb.photo.cuts');
    if (!cuts || !cuts.length) {
      return null;
    }
    return cuts.find(cut => cut.width === 960 && cut.height === 540).src;
  }

  getPreviewImage() {
    const { props } = this;
    const previewImage = new PreviewImage({
      awayTeamId: props.get('teams.away.team.id'),
      homeTeamId: props.get('teams.home.team.id')
    });
    return previewImage.getSrc();
  }

  isPreview() {
    return (
      this.props.get('status.abstractGameCode') === ItemDetail.PREVIEW_CODE
    );
  }

  open() {
    this.el.classList.add('open');
  }

  close() {
    this.el.classList.remove('close');
    if (typeof this.props.onClose === 'function') {
      this.props.onClose();
    }
    this.unBindEvents();
    this.el.remove();
  }

  openDetailView() {}

  async preRender() {
    this.gameImage = this.getImage();

    if (!this.gameImage) {
      this.gameImage = await this.getPreviewImage();
    }
    return Promise.resolve();
  }

  template() {
    const title = this.getTitle();
    const text = this.getText();
    const image = this.gameImage;

    return `<div class="item-detail">
                    <h1>${title}</h1>
                    <img src="${image}" alt="${title}"/>
                    <div class="item-detail-copy">
                        ${text}
                    </div>
                </div>`;
  }
}

export default ItemDetail;
