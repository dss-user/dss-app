import Component from '../Component/Component';
import './Item.scss';
import { isToday, format, parseISO } from 'date-fns';
import PreviewImage from '../../Services/Util/PreviewImage';
import ItemDetail from '../ItemDetail/ItemDetail';

class Item extends Component {
  // MLB preview code.
  static PREVIEW_CODE = 'P';
  static POSTPONED_STATE_CODE = 'D';

  // Initialize a variable to store a detail view.
  itemDetail;

  getTitle() {
    const { props } = this;
    return (
      props.get('content.editorial.recap.mlb.headline') ||
      `${props.get('teams.away.team.name')} at ${props.get(
        'teams.home.team.name'
      )}`
    );
  }

  getImage() {
    const { props } = this;
    const cuts = props.get('content.editorial.recap.mlb.photo.cuts');
    if (!cuts || !cuts.length) {
      return null;
    }
    return cuts.find(cut => cut.width === 960 && cut.height === 540).src;
  }

  getPreviewImage() {
    const { props } = this;
    const previewImage = new PreviewImage({
      awayTeamId: props.get('teams.away.team.id'),
      homeTeamId: props.get('teams.home.team.id')
    });
    return previewImage.getSrc();
  }

  isPreview() {
    return this.props.get('status.abstractGameCode') === Item.PREVIEW_CODE;
  }

  isPostponed() {
    return (
      this.props.get('status.codedGameState') === Item.POSTPONED_STATE_CODE
    );
  }

  getSubTitle() {
    const { props } = this;

    // Is this the second game of a double header?
    if (props.doubleHeader === 'Y' && props.gameNumber > 1) {
      return 'Game 2';
    }

    if (this.isPostponed()) {
      return `${props.get('status.detailedState')} (${props.get(
        'status.reason'
      )})`;
    }

    // Is game a preview?
    if (this.isPreview()) {
      const gameTime = parseISO(props.get('gameDate'));
      return isToday(gameTime)
        ? `Today at ${format(gameTime, 'p')}`
        : format(gameTime, "EEEE 'at' p");
    }

    const awayScore = props.get('linescore.teams.away.runs');
    const homeScore = props.get('linescore.teams.home.runs');

    return `${props.get('teams.away.team.name')} ${awayScore}, ${props.get(
      'teams.home.team.name'
    )} ${homeScore}`;
  }

  setActive() {
    this.el.classList.add('active');
  }

  setInactive() {
    this.el.classList.remove('active');
  }

  width() {
    const rect = this.el.getBoundingClientRect();
    const marginRight = getComputedStyle(this.el).marginRight;
    const marginLeft = getComputedStyle(this.el).marginLeft;
    return rect.width + parseInt(marginRight, 10) + parseInt(marginLeft, 10);
  }

  openDetailView(detailCloseCallback) {
    // Ugly way to reference the app root, but will work for now.
    const appRoot = document.querySelector('#root');

    this.itemDetail = new ItemDetail(appRoot, {
      ...this.props,
      onClose: detailCloseCallback
    });
    this.itemDetail.render();
  }

  async preRender() {
    this.gameImage = this.getImage();

    if (!this.gameImage) {
      this.gameImage = await this.getPreviewImage();
    }
    return Promise.resolve();
  }

  template() {
    const title = this.getTitle();
    const subtitle = this.getSubTitle();
    const image = this.gameImage;

    return `<div class="item">
                    <div class="scaled-item">
                        <h2>${title}</h2>
                        <img src="${image}" alt="${title}">
                        <h3>${subtitle}</h3>
                    </div>
                </div>`;
  }
}

export default Item;
