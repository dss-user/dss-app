import { get } from '../../Services/Util/Util';

export default class Component {
  constructor(parentEl, props = {}) {
    this._parentEl = parentEl;
    this._props = props || {};

    // Require a template method be defined.
    if (typeof this.template !== 'function') {
      throw new Error(
        `No template function defined on component ${this.constructor.name}`
      );
    }
  }

  set parentEl(el) {
    this._parentEl = el;
  }

  get parentEl() {
    return this._parentEl;
  }

  set props(props) {
    this._props = props;
  }

  get props() {
    // Append a helper function for deeply nested props.
    const props = this._props;
    props.get = get;

    return props;
  }

  set el(el) {
    this._el = el;
  }

  get el() {
    return this._el;
  }

  async render() {
    // Call our single lifecycle function if needed.
    if (typeof this.preRender === 'function') {
      await this.preRender();
    }

    // Build the template
    const componentTemplate = await this.template();
    let el;

    // Template can be a string or a DOM node.
    //
    // It's difficult to keep reference to elements if we create
    //  them as strings, so preference would be to use DOM objects.
    if (typeof componentTemplate === 'string') {
      const template = document.createElement('template');
      template.innerHTML = componentTemplate;
      const content = template.content;
      el = content.childNodes[0];
    } else {
      el = componentTemplate;
    }

    // Append the el to it's parent
    this._el = this.parentEl.appendChild(el);
  }
}
