import Component from '../Component/Component';
import './ItemNav.scss';
import { format } from 'date-fns';

import Api from '../../Services/Api/Api';
import Item from '../Item/Item';

class ItemNav extends Component {
  static ARROW_LEFT = 'ArrowLeft';
  static ARROW_RIGHT = 'ArrowRight';
  static ENTER = 'Enter';

  items = [];
  activeItemIndex = 0;

  constructor() {
    super(...arguments);
    this.bindEvents();
  }

  bindEvents() {
    document.addEventListener('keydown', evt =>
      this.handleKeyEvent(evt, 'Down')
    );
  }

  handleKeyEvent(evt, type) {
    if (!this.props.active) {
      return;
    }

    const navKeyCode = [
      ItemNav.ENTER,
      ItemNav.ARROW_LEFT,
      ItemNav.ARROW_RIGHT
    ].find(code => code === evt.code);

    if (navKeyCode) {
      const functionSignature = `handle${navKeyCode}${type}`;
      if (typeof this[functionSignature] !== 'function') {
        return;
      }
      this[functionSignature]();
    }
  }

  handleArrowLeftDown() {
    this.activeItemIndex = this.getNextActiveIndex(--this.activeItemIndex);
    this.setActiveChild();
  }

  handleArrowRightDown() {
    this.activeItemIndex = this.getNextActiveIndex(++this.activeItemIndex);
    this.setActiveChild();
  }

  handleEnterDown() {
    this.openDetailView();
  }

  isDetailOpen() {
    return this.detailOpen;
  }

  openDetailView() {
    this.detailOpen = true;
    this.items[this.activeItemIndex].openDetailView(() => this.setActive());
    this.setInactive();
  }

  getNextActiveIndex(newIndex) {
    if (newIndex > this.items.length - 1) {
      return 0;
    }
    if (newIndex < 0) {
      return this.items.length - 1;
    }
    return newIndex;
  }

  setActiveChild() {
    this.setXCoord(this.activeItemIndex);
    this.items
      .filter((item, idx) => idx !== this.activeItemIndex)
      .map(item => item.setInactive());
    this.items[this.activeItemIndex].setActive();
  }

  setXCoord(childIndex) {
    const minWidth = this.findChildUnScaledWidth();
    const offset = -childIndex * minWidth;
    this.sliderEl.style.transform = `translateX(${offset}px)`;
  }

  findChildUnScaledWidth() {
    return Math.min(...this.items.map(item => item.width()));
  }

  setActive() {
    // Reset the detailOpen props
    this.detailOpen = false;

    // Set the "state"
    this.props.active = true;

    // Add class
    this.el.classList.add('active');

    // Active current child
    this.items[this.activeItemIndex].setActive();

    // Scroll
    this.el.scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  }

  setInactive() {
    // Set the "state"
    this.props.active = false;

    // Remove class
    this.el.classList.remove('active');

    // Deactivate all children
    this.items.map(item => item.setInactive());
  }

  /**
   * Returns an endpoint to use in the API.
   *
   * @param date  Date    A Date instance to use in the API endpoint.
   * @returns {string}
   */
  getEndpoint(date) {
    const formattedDate = format(date, 'yyyy-MM-dd');
    return `/schedule?hydrate=linescore(matchup),game(content(editorial(recap))),decisions&date=${formattedDate}&sportId=1`;
  }

  async preRender() {
    const date = this.props.date || new Date();
    const endpoint = this.getEndpoint(date);

    this.props.data = await Api.get(endpoint);

    // Just return a resolve so element will render.
    return Promise.resolve();
  }

  getGames() {
    const {
      data: { dates }
    } = this.props;

    // For this nav we will just take the first date item.
    if (!dates || !dates.length) {
      throw new Error('No dates found in response.');
    }

    // Assume well structured data from back end.
    return dates[0].games;
  }

  async template() {
    const games = this.getGames();

    const itemNavContainerEl = document.createElement('div');
    const titleEl = document.createElement('h2');
    const itemNavEl = document.createElement('div');

    // set reference to inner container
    this.sliderEl = itemNavEl;

    itemNavContainerEl.classList.add('item-nav-container');
    titleEl.innerText = this.props.title || this.props.date || '';
    itemNavEl.classList.add('item-nav');

    // Using a for loop instead of an array fn so that elements are
    //  appended/rendered in order.
    for (let i = 0; i < games.length; ++i) {
      const item = new Item(itemNavEl, games[i]);
      await item.render();
      this.items.push(item);
    }

    // Set initial item active.
    if (this.props.active) {
      this.items[this.activeItemIndex].setActive();
      itemNavContainerEl.classList.add('active');
    }

    // Add the title to the container
    itemNavContainerEl.appendChild(titleEl);

    // Add the slider to the container;
    itemNavContainerEl.appendChild(itemNavEl);

    return itemNavContainerEl;
  }
}

export default ItemNav;
