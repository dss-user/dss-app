const Component = require('../src/Components/Component/Component').default;

describe('Base Component', function() {
  it('component without template should fail to initialize', function(done) {
    try {
      class TestComponent extends Component {}
      new TestComponent();
    } catch (e) {
      return done();
    }
    done('Should have failed initialization.');
  });

  it('component with template initializes', function(done) {
    try {
      class TestComponent extends Component {
        template() {
          return '<div>test</div>';
        }
      }
      new TestComponent();
    } catch (e) {
      return done(e);
    }
    done();
  });
});
