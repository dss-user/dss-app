const nf = require('node-fetch');
const Api = require('../src/Services/Api/Api').default;

describe('Api', function() {
  before(function() {
    global.fetch = nf;
  });

  it('api result is json', function(done) {
    Api.get('/schedule').then(res => {
      if (typeof res === 'object') {
        done();
      } else {
        done('api response is of the wrong type');
      }
    });
  });
});
